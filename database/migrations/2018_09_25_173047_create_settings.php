<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Setting;

class CreateSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $setting = new Setting;
        $setting->name = 'title';
        $setting->description = 'Title of this website.';
        $setting->value = 'SimpleCMS';
        $setting->save();

        $setting = new Setting;
        $setting->name = 'copyright_author';
        $setting->description = 'Author that is displayed within the copyright message.';
        $setting->value = 'Simple CMS';
        $setting->save();

        $setting = new Setting;
        $setting->name = 'copyright_statement';
        $setting->description = 'Rights statement that is displayed within the copyright message behind the author.';
        $setting->value = 'All Rights Reserved';
        $setting->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
