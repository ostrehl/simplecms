<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //collections describe a site that can have several children
        //a child can be another collection or a pages
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('root')->default(false); // a page is a root page if it is not a child of another page
            $table->integer('order')->nullable(); // order only apply for root pages
            $table->boolean('published')->default(false);
            $table->string('title');
            $table->text('teaser')->nullable();
            $table->string('preview_image')->nullable();
            $table->mediumText('text');

            $table->timestamps();
        });

        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published');
            $table->string('title');
            $table->text('teaser')->nullable();
            $table->string('preview_image')->nullable();
            $table->mediumText('text');

            $table->timestamps();
        });

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published');
            $table->string('title');
            $table->text('teaser')->nullable();
            $table->mediumText('text');
            $table->string('size');
            $table->string('type');
            $table->string('url');

            $table->unsignedBigInteger('downloads')->nullable();
            $table->timestamps();
          });

          Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published');
            $table->string('title');
            $table->text('teaser')->nullable();
            $table->mediumText('text');
            $table->string('size');
            $table->string('type');
            $table->string('url');
            $table->timestamps();
          });

        Schema::create('page_childs', function (Blueprint $table) {
            $table->unsignedInteger('page_id');
            $table->foreign('page_id')
                ->references('id')
                ->on('pages')
                ->onDelete('cascade');
            $table->string('page_child_type'); // page, article, file, image
            $table->unsignedInteger('page_child_id');
            $table->unsignedInteger('order');
            $table->timestamps();

            $table->primary(['page_id', 'page_child_type', 'page_child_id'], 'page_has_childrens_primary');
        });

        Schema::create('article_childs', function (Blueprint $table) {
            $table->unsignedInteger('article_id');
            $table->foreign('article_id')
                ->references('id')
                ->on('articles')
                ->onDelete('cascade');
            $table->string('article_child_type'); // file, image
            $table->unsignedInteger('article_child_id');
            $table->unsignedInteger('order');
            $table->timestamps();

            $table->primary(['article_id', 'article_child_type', 'article_child_id'], 'article_has_childrens_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_childs');
        Schema::dropIfExists('article_childs');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('articles');
        Schema::dropIfExists('files');
        Schema::dropIfExists('images');
    }
}
