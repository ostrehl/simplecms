<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateDefaultUserAndRights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'view_roles']);
        Permission::create(['name' => 'view_permission']);
        Permission::create(['name' => 'view_user']);
        Permission::create(['name' => 'create_user']);
        Permission::create(['name' => 'update_user']);
        Permission::create(['name' => 'delete_user']);

        $role = Role::create(['name' => 'user_manager']);
        $role->givePermissionTo('view_roles');
        $role->givePermissionTo('view_permission');
        $role->givePermissionTo('view_user');
        $role->givePermissionTo('create_user');
        $role->givePermissionTo('update_user');
        $role->givePermissionTo('delete_user');

        Permission::create(['name' => 'create_page']);
        Permission::create(['name' => 'update_page']);
        Permission::create(['name' => 'delete_page']);
        Permission::create(['name' => 'create_article']);
        Permission::create(['name' => 'update_article']);
        Permission::create(['name' => 'delete_article']);
        Permission::create(['name' => 'publish_article']);
        Permission::create(['name' => 'unpublish_article']);

        $role = Role::create(['name' => 'content_manager']);
        $role->givePermissionTo('create_page');
        $role->givePermissionTo('update_page');
        $role->givePermissionTo('delete_page');
        $role->givePermissionTo('create_article');
        $role->givePermissionTo('update_article');
        $role->givePermissionTo('delete_article');
        $role->givePermissionTo('publish_article');
        $role->givePermissionTo('unpublish_article');

        $role = Role::create(['name' => 'article_editor']);
        $role->givePermissionTo('create_article');
        $role->givePermissionTo('update_article');
        $role->givePermissionTo('delete_article');
        $role->givePermissionTo('publish_article');
        $role->givePermissionTo('unpublish_article');

        Permission::create(['name' => 'update_page_setting']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('update_page_setting');
        $role->givePermissionTo('view_roles');
        $role->givePermissionTo('view_permission');
        $role->givePermissionTo('view_user');
        $role->givePermissionTo('create_user');
        $role->givePermissionTo('update_user');
        $role->givePermissionTo('delete_user');
        $role->givePermissionTo('create_page');
        $role->givePermissionTo('update_page');
        $role->givePermissionTo('delete_page');
        $role->givePermissionTo('create_article');
        $role->givePermissionTo('update_article');
        $role->givePermissionTo('delete_article');
        $role->givePermissionTo('publish_article');
        $role->givePermissionTo('unpublish_article');

        // permission that give super cow power
        Permission::create(['name' => 'create_role']);
        Permission::create(['name' => 'update_role']);
        Permission::create(['name' => 'delete_role']);
        Permission::create(['name' => 'create_permission']);
        Permission::create(['name' => 'update_permission']);
        Permission::create(['name' => 'delete_permission']);

        $role = Role::create(['name' => 'super_admin']);
        $role->givePermissionTo(Permission::all());

        $user = new App\User();
        $user->password = Hash::make('top_secret');
        $user->name = "Super Admin";
        $user->email = 'o.strehl@forst-jagd-edv.de';
        $user->save();

        $user->assignRole('super_admin');

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //TODO delete created permissions and roles
        // Role::destroy('user_manager');
        // Role::destroy('content_manager');
        // Role::destroy('article_editor');
        // Role::destroy('admin');
        // Role::destroy('super_admin');
        //
        // Permission::destroy(['name' => 'view_roles']);
        // Permission::destroy(['name' => 'view_permission']);
        // Permission::destroy(['name' => 'view_user']);
        // Permission::destroy(['name' => 'create_user']);
        // Permission::destroy(['name' => 'update_user']);
        // Permission::destroy(['name' => 'delete_user']);
        //
        // Permission::destroy(['name' => 'create_page']);
        // Permission::destroy(['name' => 'update_page']);
        // Permission::destroy(['name' => 'delete_page']);
        // Permission::destroy(['name' => 'update_article']);
        // Permission::destroy(['name' => 'delete_article']);
        // Permission::destroy(['name' => 'publish_article']);
        // Permission::destroy(['name' => 'unpublish_article']);
        //
        // Permission::create(['name' => 'update_page_setting']);
        //
        // Permission::destroy(['name' => 'create_role']);
        // Permission::destroy(['name' => 'update_role']);
        // Permission::destroy(['name' => 'delete_role']);
        // Permission::destroy(['name' => 'create_permission']);
        // Permission::destroy(['name' => 'update_permission']);
        // Permission::destroy(['name' => 'delete_permission']);

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
    }
}
