# Simple Content Management System (SimpleCMS) based on Laravel 5
Simple content management system based on Laravel 5

Used components
* spatie/permissions,
* laravel/crud
* laravelCollective

## License
The SimpleCMS is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Distinction from other CMS
Fast and easy setup
Basic CMS functionality included
Based on a solid, extensible  architecture
Programming knowledge needed for setup and customization
Easy content management by usage of a simple tree structure
Meaningful URLs


## Installation

### Composer

### Configuration

### Migrations

## User / Roles / Permissions

## CMS Concept

### Page collection
* title
* teaser
* preview image
* published
* articles
* files
* images

### Article
* title
* teaser
* preview image
* published
* text
* images
* files

### File
* title
* teaser
* preview image
* published

### Image
* title
* teaser
* published
* description
* image
