<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/member', 'MemberController@index');

Route::get('/contact', function () {
    return view('contact');
});

Route::post('/contact/submit', 'MessagesController@submit');

// login // register
Auth::routes();

// logout
Route::get('/logout', function () {
  Auth::logout();
  return redirect('/home');
});

// Content management area
Route::group(['middleware' =>
['permission:create_page|update_page|delete_page|create_article|update_article|delete_article|publish_article|unpublish_article']], function () {
  Route::get('/cms', function () {
      return view('cms/dashboard');
  });

  Route::group(['middleware' =>
  ['permission:create_page|update_page|delete_page']], function () {
    Route::resource('cms/page', 'Cms\\PageController');
  });

});

// Admin area
Route::group(['middleware' =>
['permission:view_user|view_roles|view_permission|update_page_setting']], function () {
  Route::get('/admin', function () {
      return view('admin/dashboard');
  });

  Route::group(['middleware' => ['permission:view_user']], function () {
      Route::resource('admin/user', 'Admin\\UserController');
  });

  Route::group(['middleware' => ['permission:view_roles']], function () {
      Route::resource('admin/role', 'Admin\\RoleController');
  });

  Route::group(['middleware' => ['permission:view_permission']], function () {
      Route::resource('admin/permission', 'Admin\\PermissionController');
  });

  Route::group(['middleware' => ['permission:update_page_setting']], function () {
    Route::resource('admin/setting', 'Admin\\SettingController');
  });
});
