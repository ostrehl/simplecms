<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    @php
      $title = \App\Setting::where('name','title')->get()[0]
    @endphp
  <title> {{ $title->value}} </title>
  <link rel="stylesheet" href="/css/app.css"></link>
</head>
<body>
  @include('inc.navbar')

  @include('inc.messages')

  @yield('content')


  @include('inc.footer')
  <script src="/js/app.js"></script>
</body>
</html>
