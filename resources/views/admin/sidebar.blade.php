<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Administration
        </div>

        <div class="card-body">

             <ul role="tablist"><!--class="nav" -->

                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                        Dashboard
                    </a>
                </li>

                @can('update_page_setting')
                <li role="presentation">
                    <a href="{{ url('/admin/setting') }}">
                        Setting
                    </a>
                </li>
                @endcan
                @can('view_user')
                  <li role="presentation">
                      <a href="{{ url('/admin/user') }}">
                          User
                      </a>
                  </li>
                @endcan
                @can('view_roles')
                  <li role="presentation">
                      <a href="{{ url('/admin/role') }}">
                          Role
                      </a>
                  </li>
                @endcan
                @can('view_permission')
                  <li role="presentation">
                      <a href="{{ url('/admin/permission') }}">
                          Permission
                      </a>
                  </li>
                @endcan
            </ul>
        </div>
    </div>
</div>
