@section('navbar')
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="/">{{ $title->value}}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExample04">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/member">Member <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown04">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contact">Contact</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      @if (Auth::check())
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="dropdown_user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</a>
          <div class="dropdown-menu" aria-labelledby="dropdown_user">
            <a class="dropdown-item" href={{ route('logout') }}>Logout</a>
            @can('create_page', 'update_page', 'delete_page', 'create_article', 'update_article', 'delete_article', 'publish_article', 'unpublish_article')
              <a class="dropdown-item" href="/cms">Content Management</a>
            @endcan
            @can('view_user', 'view_roles', 'view_permission', 'update_page_setting')
              <a class="dropdown-item" href='/admin'>Administration</a>
            @endcan
          </div>
        </li>
      @else
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="dropdown_login" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('Login') }}</a>
          <div class="dropdown-menu" aria-labelledby="dropdown_login" style="padding:17px;">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" id="formLogin">
              @csrf
              <div class="form-group row col-md-6">
                <input id="email" type="email" name="email" required autofocus>
              </div>
              <div class="form-group row col-md-6">
                <input id="password" type="password" name="password" required>
              </div>
              <div class="form-group row col-md-6">
                <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
              </div>
            </form>
          </div>
        </li>
      @endif
      <li class="nav-item active">
        <form class="form-inline my-2 my-md-0">
          <input class="form-control" type="text" placeholder="Search">
        </form>
      </li>
    </ul>
  <div>
</nav>
