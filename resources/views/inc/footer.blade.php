@section('footer')
@php
  $copyright_author = \App\Setting::where('name','copyright_author')->get()[0];
  $copyright_statement = \App\Setting::where('name', 'copyright_statement')->get()[0];
@endphp

<footer id="footer" class="text-center">
<p> &copy; 2018 - {{ now()->year }} {{ $copyright_author->value }} {{ $copyright_statement->value }}</p>
</footer>
