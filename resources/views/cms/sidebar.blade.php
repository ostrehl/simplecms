<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Content Management
        </div>

        <div class="card-body">

             <ul role="tablist"><!--class="nav" -->

                <li role="presentation">
                    <a href="{{ url('/cms') }}">
                        Dashboard
                    </a>
                </li>
                @can('create_page', 'update_page', 'delete_page')
                <li role="presentation">
                    <a href="{{ url('/cms/page') }}">
                        Pages
                    </a>
                </li>
                @endcan

            </ul>
        </div>
    </div>
</div>
