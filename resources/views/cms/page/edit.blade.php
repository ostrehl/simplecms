@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('cms.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Page #{{ $page->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/cms/page') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($page, [
                            'method' => 'PATCH',
                            'url' => ['/cms/page', $page->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('cms.page.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
